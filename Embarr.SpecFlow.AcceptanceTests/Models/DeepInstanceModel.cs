﻿using System.Collections.Generic;

namespace Embarr.SpecFlow.AcceptanceTests.Models
{
    public class DeepInstanceModel
    {
        public string Name { get; set; }

        public decimal Dec { get; set; }

        public decimal? NullDec { get; set; }

        public double Doub { get; set; }

        public List<DeepInstanceModel> Coll { get; set; }

        public DeepInstanceModel Child { get; set; }

        public List<string> Strings { get; set; }

        public List<int> Ints { get; set; }

        public Dictionary<string, string> Dic { get; set; }

        public DeepInstanceModel[] MyArray { get; set; }

        public int[] MyIntArray { get; set; }

        public string[] MyStringArray { get; set; }
    }
}
