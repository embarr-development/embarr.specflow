﻿namespace Embarr.SpecFlow.Extensions.Models
{
    internal enum PropertyType
    {
        Standard,
        KeyedOrIndexer
    }
}