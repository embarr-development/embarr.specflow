# Embarr.SpecFlow #

This is a set of useful extensions and helpers when using SpecFlow

For detailed documentation including features not detailed here please visit the [Embarr Development Labs](http://embarr-development.co.uk/labs/) page.

## table.CreateDeepInstance<DeepInstanceModel>()##
This method will instantiate a deep complex object and handles.
Please see below for example usage.

## table.ValidateDeepInstance<DeepInstanceModel>(instance) ##
This method will validate an instance against the values within a table.
It will validate deep instances, please see the below example usage.

## Context Services ##

* ScenarioContextService
* FeatureContextService
* GlobalContextService

Each of these services contain the same set of useful helper methods for dealing with SpecFlow test contexts.

### Context ###
Returns the underlying SpecFlow context (it is basically a Dictionary<string, object>

### SaveValue ###
Saves an instance to the context keyed with the full name of the instance type.
Includes an overload to set a custom key.

### SaveToList ###
Saves an instance to a list on the context keyed with the full name of the instance type prefixed with list_.
Includes an overload to set a custom key.

### GetValue ###
Gets an instance from the context keyed with the full name of the instance type.
Includes an overload to get an instance using a custom key.

### GetList ###
Gets an instance of a list from the context keyed with the full name of the instance type prefixed with list_.
Includes an overload to get an list using a custom key.

### ClearValue ###
Clears a value from the context keyed with the full name of the instance type.
Includes an overload to clear a value with a custom key.

## Examples Usage ##

    Scenario: Create deep model successfully
	    When the following deep properties are hydrated:
	    | property                   | value                |
	    | Name                       | My Index 1           |
	    | Dec                        | 1.2                  |
	    | NullDec                    | 1.2                  |
	    | Doub                       | 0.8                  |
	    | Child.Name                 | A name               |
	    | Child.Dec                  | 3.2                  |
	    | Strings[0]                 | String 1             |
	    | Strings[1]                 | String 2             |
	    | Ints[0]                    | 123                  |
	    | Ints[1]                    | 234                  |
	    | Dic["MyKey"]               | My Value             |
	    | Dic["MyKey3"]              | My Value 3           |
	    | Coll[0].Dic["MyKey1"]      | My Deeper Dictionary |
	    | Coll[0].Name               | My Coll 0 Name       |
	    | Coll[0].Dec                | 3.4                  |
	    | Coll[1].Name               | My Coll 1 Name       |
	    | Coll[1].Dec                | 4.5                  |
	    | Coll[1].Coll[0].Dec        | 10.99                |
	    | Child.Coll[0].Dec          | 11.99                |
	    | Child.Coll[0].Coll[0].Name | V.Deep               |
	    | MyArray[0].Name            | Array 1              |
	    | MyArray[0].Doub            | 12.34                |
	    | MyArray[1].Name            | Array 2              |
	    | MyArray[1].Doub            | 56.78                |
	    | MyIntArray[0]              | 123                  |
	    | MyIntArray[1]              | 234                  |
	    | MyStringArray[0]           | MyStr1               |
	    | MyStringArray[1]           | MyStr2               |

    [When(@"the following deep properties are hydrated:")]
    public void WhenTheFollowingDeepPropertiesAreHydrated(Table table)
    {
        var deepModel = table.CreateDeepInstance<DeepInstanceModel>();

        var result = table.ValidateDeepInstance(deepModel);

        result.AllMatch.ShouldBeTrue();
    }

     public class DeepInstanceModel
    {
        public string Name { get; set; }

        public decimal Dec { get; set; }

        public decimal? NullDec { get; set; }

        public double Doub { get; set; }

        public List<DeepInstanceModel> Coll { get; set; }

        public DeepInstanceModel Child { get; set; }

        public List<string> Strings { get; set; }

        public List<int> Ints { get; set; }

        public Dictionary<string, string> Dic { get; set; }

        public DeepInstanceModel[] MyArray { get; set; }

        public int[] MyIntArray { get; set; }

        public string[] MyStringArray { get; set; }
    }	

## Current limitations ##

All complex classes must have a parameterless constructor